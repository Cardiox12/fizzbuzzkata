# FizzBuzz

Specifications
1. La fonction doit retourner la chaine FizzBuzz quand divisible par 3 et par 5
2. La fonction doit retourner la chaine Fizz quand divisible par 3
3. La fonction doit retourner la chaine Buzz quand divisible par 5 
4. La fonction doit retourner le parametre sous forme de chaine quand il ne rempli pas les conditions de **1**, **2** ou **3**