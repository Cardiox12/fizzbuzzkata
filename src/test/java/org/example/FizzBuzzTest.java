package org.example;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class FizzBuzzTest {
    @Test
    public void Should_return_the_number_as_a_string_when_passing_number_not_divisible_by_3_or_5() {
        int [] numbers = {1, 7, 11};
        String [] expected = {"1", "7", "11"};
        FizzBuzz fizzbuzz = new FizzBuzz();

        for (int i = 0; i < numbers.length; i++) {
            assertEquals(expected[i], fizzbuzz.print(numbers[i]));
        }
    }

    @Test
    public void Should_return_fizz_when_passing_number_3(){

    }
}
